<?php

namespace App\Controller;

use OpenAI;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

class ApiController extends AbstractController
{
    #[Route('/openai', name: 'app_prompt', methods: ["POST"])]
    public function prompt(Request $request): JsonResponse
    {
        $client = OpenAI::client($_ENV['OPENAI_APIKEY']);

        // // Récupération des variables POST
        $post = $request->toArray();
        $nbDays = $post["nbDays"];
        $start = $post["start"];
        $end = $post["end"];
        $theme = $post["theme"] != "" ? $post["theme"] : "touristique";

        $prompt = "Peux tu me donner un itinéraire " . $theme . ", entre " . $start . " et " . $end . " en " . $nbDays . " étapes avec 2 activités possibles pour chaque étape, avec une description brève, la durée en heures et la géolocalisation des activités en français. Pour chaque ville il me faudra obligatoirement la latitude et la longitude. La réponse devra obligatoirement être au format JSON sans aucun commentaire dans le format {itinerary:[{city,geolocalisation:{latitude,longitude},activities:[name,description,duration:'x heures',location:{latitude,longitude}]}]}";
        $res = $client->chat()->create([
            'model' => 'gpt-3.5-turbo',
            'messages' => [
                ['role' => 'user', 'content' => $prompt],
            ]
        ]);
        $result = json_decode($res->choices[0]->message->content);
        $itinerary = $result->itinerary;
        return new JsonResponse($itinerary);
    }

    #[Route('/ors', name: 'app_ors', methods: ["POST"])]
    public function ors(Request $request): JsonResponse
    {
        $positions = $request->toArray();
        // Créez une instance de Guzzle client
        $client = new \GuzzleHttp\Client([ 'verify' => false ]);

        // Requête POST à l'API OpenRouteService
        $response = $client->request('POST', 'https://api.openrouteservice.org/v2/directions/driving-car', [
            'headers' => [
                'Accept' => 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
                'Authorization' => '5b3ce3597851110001cf6248460367fa3bea478c957301aa2d8aa96c',
                'Content-Type' => 'application/json; charset=utf-8',
            ],
            'json' => [
                'coordinates' => $positions['coordinates'],
                'instructions' => false,
                'language' => 'fr',
                'units' => 'km'
            ]
        ]);

        // Récupérez le contenu de la réponse
        $content = json_decode($response->getBody()->getContents());
        // Retournez le contenu comme réponse Symfony
        return new JsonResponse($content->routes[0]);
    }
}
